# -*- mode: python; coding: utf-8; -*-

"""Downloading the files from FTP to PC with experiment"""

import time
from common.ftp_operations import os, ftplib, ftp_listdir, ftp_move_from
from common.variables import project_dir, ip_address, ftp_login, ftp_password, ftp_dir_subm


def main():
    moveto = os.path.join(project_dir, 'submissions/')
    control_files = ['init.m', 'control.m', 'observer.m']

    while True:
        files = ftp_listdir(ip_address, ftp_login, ftp_password, ftp_dir_subm)
        print(files)
        if len(files) == 1:
            time.sleep(5)
        if len(files) > 0:

            folder = os.path.join(moveto, files[0] +'\\')
            os.mkdir(folder)

            # moving the files from FTP to some local folder
            for i in control_files:
                ftp_move_from(ip_address, ftp_login, ftp_password, ftp_dir_subm+files[0], i, folder)

            # removing the directory from FTP
            ftp = ftplib.FTP(ip_address)
            ftp.login(user=ftp_login, passwd=ftp_password)
            ftp.cwd(ftp_dir_subm)
            ftp.rmd(files[0])
            ftp.quit()
            print('FTP file transfer is successful!')
        else:
            time.sleep(20)
            print('!')


if __name__ == '__main__':
    main()
