# -*- mode: python; coding: utf-8; -*-

"""Executing of experiment, archivation of the results and calculation of control quality"""

import time
from common.ftp_operations import os, ftplib, ftp_listdir, ftp_move_from, ftp_delete_from
from common.variables import project_dir, ip_address, ftp_login, ftp_password, ftp_dir_subm, ftp_dst

from common.exp_processing_operations import os, shutil, time, exp_execution, make_archive, listdir_nohidden
from common.variables import project_dir, database_adr, matlab_path
import sqlalchemy as db
from sqlalchemy import MetaData, Table, Column, Integer, String, DateTime, Float  # create_engine
# from datetime import datetime as dt
import re


def main():
    moveto = os.path.join(project_dir, 'submissions/')
    subm_path = moveto
    moveto_path = os.path.join(project_dir, 'in_queue/')
    completed_path = os.path.join(project_dir, 'completed/')
    completed_rasp_path = os.path.join(project_dir, 'completed_rasp/')
    eventfile_dst_path = os.path.join(project_dir, 'timer.txt')
    control_files = ['init.m', 'control.m', 'observer.m']
    while True:
        #'''
        ftp_files = ftp_listdir(ip_address, ftp_login, ftp_password, ftp_dir_subm)
        print(ftp_files)
        if len(ftp_files) == 1:
            time.sleep(5)
        if len(ftp_files) > 0:
            folder = os.path.join(moveto, ftp_files[0] + '\\')
            os.mkdir(folder)

            # moving the files from FTP to some local folder
            for i in control_files:
                ftp_move_from(ip_address, ftp_login, ftp_password, ftp_dir_subm + ftp_files[0], i, folder)

            # removing the directory from FTP
            ftp = ftplib.FTP(ip_address)
            ftp.login(user=ftp_login, passwd=ftp_password)
            ftp.cwd(ftp_dir_subm)
            ftp.rmd(ftp_files[0])
            ftp.quit()
            print('FTP file transfer is successful!')
        #'''
        # working locally now
        files = listdir_nohidden(subm_path)
        #######
#subm_path = os.path.join('C:/Users/Fiodar Hancharou/Desktop/ControlHub/submission_video_server/', 'submission_files')
        #files = listdir_nohidden(subm_path)
        #######
        print(files)
        if len(files) == 1:
            time.sleep(2)
        if len(files) > 0:
            time.sleep(1)
            src = os.path.join(subm_path, files[0])
            dst = os.path.join(moveto_path, files[0]+'\\')
            result_local = os.path.join(completed_path, files[0])
            result_remote = os.path.join(completed_rasp_path, files[0])

            # moving submission files to the queue
            shutil.move(src, dst)

            # initialisation of the db
            engine = db.create_engine(database_adr, echo=True)  # "sqlite:///db/lab.db"
            connection = engine.connect()
            meta = db.MetaData()

            submissions = db.Table('Submissions', meta, autoload=True, autoload_with=engine)

            control_quality = Table('control_quality', meta, autoload=True, autoload_with=engine)
            '''
            control_quality = Table('control_quality', meta,
                                    Column('id', Integer, primary_key=True),
                                    Column('username', String(30), index=True),
                                    Column('folder', String(30)),
                                    Column('date', DateTime),
                                    Column('l2norm_theta_sim', Float),
                                    Column('l2norm_alpha_sim', Float),
                                    Column('l2norm_d_theta_sim', Float),
                                    Column('l2norm_d_alpha_sim', Float),
                                    Column('l2norm_u_sim', Float),

                                    Column('linorm_theta_sim', Float),
                                    Column('linorm_alpha_sim', Float),
                                    Column('linorm_d_theta_sim', Float),
                                    Column('linorm_d_alpha_sim', Float),
                                    Column('linorm_u_sim', Float),

                                    Column('l2norm_theta_exp', Float),
                                    Column('l2norm_alpha_exp', Float),
                                    Column('l2norm_d_theta_exp', Float),
                                    Column('l2norm_d_alpha_exp', Float),
                                    Column('l2norm_u_exp', Float),

                                    Column('linorm_theta_exp', Float),
                                    Column('linorm_alpha_exp', Float),
                                    Column('linorm_d_theta_exp', Float),
                                    Column('linorm_d_alpha_exp', Float),
                                    Column('linorm_u_exp', Float),
                                    Column('exp_type', String(50)))
            '''
            query = submissions.select()
            query = query.where(submissions.c.folder == files[0])
            ResultProxy = connection.execute(query)
            ResultSet = ResultProxy.fetchall()
            exp_type = ResultSet[0][2]

            if exp_type == 'stabilisation':
                sim_path = os.path.join(matlab_path, 'Type_1/qube_sim/')
                exp_path = os.path.join(matlab_path, 'Type_1/qube_exp/')
            if exp_type == 'step_signal':
                sim_path = os.path.join(matlab_path, 'Type_2/qube_sim/')
                exp_path = os.path.join(matlab_path, 'Type_2/qube_exp/')
            if exp_type == 'ref_trajectory':
                sim_path = os.path.join(matlab_path, 'Type_3/qube_sim/')
                exp_path = os.path.join(matlab_path, 'Type_3/qube_exp/')

            # 'C:/Users/Fiodar Hancharou/Desktop/ControlHub/submission_video_server/mylab/timer/timer.txt'
            txt_msg = 'Status: Task "{}" is executing. \\n In a Queue: {} submission(s).'.format(files[0], len(ftp_files))
            print(txt_msg)
            with open(eventfile_dst_path, 'w') as event_wrt:
                event_wrt.write(txt_msg)

            # experiment execution
            exp_execution(dst, sim_path, exp_path, control_files)

            query = control_quality.select()
            ResultProxy = connection.execute(query)
            ctrl_qual_data = ResultProxy.fetchall()
            print('Currently, there are {} rows in the database'.format(len(ctrl_qual_data)))
            if len(ctrl_qual_data) > 700:
                query = 'with tmp as (select \
                min(mdm1.l2norm_theta_sim) min_l2norm_theta_sim, \
                min(mdm1.l2norm_alpha_sim) min_l2norm_alpha_sim, \
                min(mdm1.l2norm_d_theta_sim) min_l2norm_d_theta_sim, \
                min(mdm1.l2norm_d_alpha_sim) min_l2norm_d_alpha_sim, \
                min(mdm1.l2norm_u_sim) min_l2norm_u_sim, \
                min(mdm1.linorm_theta_sim) min_linorm_theta_sim, \
                min(mdm1.linorm_alpha_sim) min_linorm_alpha_sim, \
                min(mdm1.linorm_d_theta_sim) min_linorm_d_theta_sim, \
                min(mdm1.linorm_d_alpha_sim) min_linorm_d_alpha_sim, \
                min(mdm1.linorm_u_sim) min_linorm_u_sim, \
                min(mdm1.l2norm_theta_exp) min_l2norm_theta_exp, \
                min(mdm1.l2norm_alpha_exp) min_l2norm_alpha_exp, \
                min(mdm1.l2norm_d_theta_exp) min_l2norm_d_theta_exp, \
                min(mdm1.l2norm_d_alpha_exp) min_l2norm_d_alpha_exp, \
                min(mdm1.l2norm_u_exp) min_l2norm_u_exp, \
                min(mdm1.linorm_theta_exp) min_linorm_theta_exp, \
                min(mdm1.linorm_alpha_exp) min_linorm_alpha_exp, \
                min(mdm1.linorm_d_theta_exp) min_linorm_d_theta_exp, \
                min(mdm1.linorm_d_alpha_exp) min_linorm_d_alpha_exp, \
                min(mdm1.linorm_u_exp) min_linorm_u_exp \
                from control_quality mdm1), tmp1 as (select \
                mdm2.id, row_number() over(order by mdm2.date) rn \
                from control_quality mdm2 join tmp  \
                where 1=1 and \
                mdm2.l2norm_theta_sim!=tmp.min_l2norm_theta_sim and \
                mdm2.l2norm_alpha_sim!=tmp.min_l2norm_alpha_sim and \
                mdm2.l2norm_d_theta_sim!=tmp.min_l2norm_d_theta_sim and \
                mdm2.l2norm_d_alpha_sim!=tmp.min_l2norm_d_alpha_sim and \
                mdm2.l2norm_u_sim!=tmp.min_l2norm_u_sim and \
                mdm2.linorm_theta_sim!=tmp.min_linorm_theta_sim and \
                mdm2.linorm_alpha_sim!=tmp.min_linorm_alpha_sim and \
                mdm2.linorm_d_theta_sim!=tmp.min_linorm_d_theta_sim and \
                mdm2.linorm_d_alpha_sim!=tmp.min_linorm_d_alpha_sim and \
                mdm2.linorm_u_sim!=tmp.min_linorm_u_sim and \
                mdm2.l2norm_theta_exp!=tmp.min_l2norm_theta_exp and \
                mdm2.l2norm_alpha_exp!=tmp.min_l2norm_alpha_exp and \
                mdm2.l2norm_d_theta_exp!=tmp.min_l2norm_d_theta_exp and \
                mdm2.l2norm_d_alpha_exp!=tmp.min_l2norm_d_alpha_exp and \
                mdm2.l2norm_u_exp!=tmp.min_l2norm_u_exp and \
                mdm2.linorm_theta_exp!=tmp.min_linorm_theta_exp and \
                mdm2.linorm_alpha_exp!=tmp.min_linorm_alpha_exp and \
                mdm2.linorm_d_theta_exp!=tmp.min_linorm_d_theta_exp and \
                mdm2.linorm_d_alpha_exp!=tmp.min_linorm_d_alpha_exp and \
                mdm2.linorm_u_exp!=tmp.min_linorm_u_exp) \
                select id from tmp1 where rn=1;'
                ResultProxy = connection.execute(query)
                id_del = ResultProxy.fetchall()
                print('Query result: ', id_del)
                query = f'delete from control_quality where id = {id_del[0][0]};'
                print('Need to delete the following row: ', query)
                query_folder = 'select folder from control_quality where id= {}'.format(id_del[0][0])
                ResultProxy = connection.execute(query_folder)
                file_del = '{}.zip'.format(ResultProxy.fetchall()[0][0])
                print('{} will be deleted soon'.format(file_del))
                if id_del[0][0]:
                    ResultProxy = connection.execute(query)
                    print('Row with id = {} was deleted successfully!'.format(query))
                    #ftp_delete_from(ip_address, ftp_login, ftp_password, ftp_dst, file_del)

            with open(os.path.join(dst, "performance_sim.txt")) as perf:
                res_sim = perf.read()
            with open(os.path.join(dst, "performance_exp.txt")) as perf:
                res_exp = perf.read()

            # split the filename to name and date
            pattern = '!'
            user_split = re.split(pattern, files[0])

            ins = control_quality.insert().values(username=user_split[0], date=user_split[1], folder=files[0],
                                                  l2norm_theta_sim=float(res_sim.split()[0]),
                                                  l2norm_alpha_sim=float(res_sim.split()[1]),
                                                  l2norm_d_theta_sim=float(res_sim.split()[2]),
                                                  l2norm_d_alpha_sim=float(res_sim.split()[3]),
                                                  l2norm_u_sim=float(res_sim.split()[4]),

                                                  linorm_theta_sim=float(res_sim.split()[5]),
                                                  linorm_alpha_sim=float(res_sim.split()[6]),
                                                  linorm_d_theta_sim=float(res_sim.split()[7]),
                                                  linorm_d_alpha_sim=float(res_sim.split()[8]),
                                                  linorm_u_sim=float(res_sim.split()[9]),

                                                  l2norm_theta_exp=float(res_exp.split()[0]),
                                                  l2norm_alpha_exp=float(res_exp.split()[1]),
                                                  l2norm_d_theta_exp=float(res_exp.split()[2]),
                                                  l2norm_d_alpha_exp=float(res_exp.split()[3]),
                                                  l2norm_u_exp=float(res_exp.split()[4]),

                                                  linorm_theta_exp=float(res_exp.split()[5]),
                                                  linorm_alpha_exp=float(res_exp.split()[6]),
                                                  linorm_d_theta_exp=float(res_exp.split()[7]),
                                                  linorm_d_alpha_exp=float(res_exp.split()[8]),
                                                  linorm_u_exp=float(res_exp.split()[9]),
                                                  exp_type=exp_type)

            result = connection.execute(ins)
            print('Results are added to the database')

            # archiving the results
            make_archive(dst, result_local)
            make_archive(dst, result_remote)

            # deletion of submission folder from the queue
            try:
                shutil.rmtree(dst)
            except OSError as e:
                print("Error: %s - %s." % (e.filename, e.strerror))

            #######
            # shutil.move(result_remote+'.zip', os.path.join('C:/Users/Fiodar Hancharou/Desktop/ControlHub/submission_video_server/', 'mylab/static/exp_results/'))
            #######
            print('Done!!!!!!!')
            time.sleep(10)
        else:
            # 'C:/Users/Fiodar Hancharou/Desktop/ControlHub/submission_video_server/mylab/timer/timer.txt'
            with open(eventfile_dst_path,'w') as event_wrt:
                event_wrt.write('Status: idle')
            time.sleep(20)
            print('!')


if __name__ == '__main__':
    main()
