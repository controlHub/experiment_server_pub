# -*- mode: python; coding: utf-8; -*-

"""Current script update the text file with message that will be displayed in the experiment video"""

from common.ftp_operations import os, ftp_copy_paste_to
from common.variables import project_dir, ip_address, ftp_login, ftp_password, timer_file, timer_copy_to
import time

copy_from = os.path.join(project_dir, '')


def main():
    while True:
        ftp_copy_paste_to(ip_address, ftp_login, ftp_password, copy_from, timer_file, timer_copy_to, timer_file)
        #print('OK', end=" ")
        time.sleep(2)


if __name__ == '__main__':
    main()
