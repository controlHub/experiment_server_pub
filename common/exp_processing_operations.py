# -*- mode: python; coding: utf-8; -*-

"""Functions for experiment execution, archivation and control quality estimation"""

import shutil
import time
import subprocess
import os
from zipfile import ZipFile
from os.path import basename
import re
import wmi


def exp_execution(res_saving_path, sim_path, exp_path, matlab_files):

    for i in matlab_files:
        with open(os.path.join(res_saving_path, i), 'rb') as rfile:
            text = rfile.read()
        with open(os.path.join(sim_path, i), 'wb+') as wfile_sim:
            wfile_sim.write(text)
        with open(os.path.join(exp_path, i), 'wb+') as wfile_exp:
            wfile_exp.write(text)
    print('Matlab files are red and written to the sim and exp directories!')

    sim_run_path = os.path.join(sim_path, 'start_sim.m')
    cmd_l = 'matlab -nodisplay -nosplash -nodesktop -r \"run(\'{}\');exit;\";'.format(sim_run_path)
    subprocess.Popen(cmd_l, shell=True)

    #######
    # shutil.copy('C:/Users/Fiodar Hancharou/Desktop/ControlHub/experiment_server/dummy/error_code_sim.txt', sim_path)
    # shutil.copy('C:/Users/Fiodar Hancharou/Desktop/ControlHub/experiment_server/dummy/performance_sim.txt', sim_path)
    # shutil.copy('C:/Users/Fiodar Hancharou/Desktop/ControlHub/experiment_server/dummy/results_sim.m', sim_path)
    #######

    time.sleep(65)  # 65
    kill_matlab('java.exe')
    time.sleep(5)
    print('Simulation is finished!')

    shutil.move(os.path.join(sim_path, 'error_code_sim.txt'), res_saving_path)
    shutil.move(os.path.join(sim_path, 'performance_sim.txt'), res_saving_path)
    try:
        shutil.move(os.path.join(sim_path, 'results_sim.m'), res_saving_path)
    except:
        print('There is no results_exp.m')
    print('Simulation Results are moved to the user folder!')

    with open(os.path.join(res_saving_path, "error_code_sim.txt")) as error_sim:
        error_msg_sim = error_sim.read()
        if error_msg_sim[0] == "0":
            print('Simulation is finished without errors!')

            exp_run_path = os.path.join(exp_path, 'start_exp.m')
            cmd_l = 'matlab -nodisplay -nosplash -nodesktop -r \"run(\'{}\');exit;\";'.format(exp_run_path)
            subprocess.Popen(cmd_l, shell=True)

            '''
            #######
            shutil.copy('C:/Users/Fiodar Hancharou/Desktop/ControlHub/experiment_server/dummy/error_code_exp.txt',
                        exp_path)
            shutil.copy('C:/Users/Fiodar Hancharou/Desktop/ControlHub/experiment_server/dummy/performance_exp.txt',
                        exp_path)
            shutil.copy('C:/Users/Fiodar Hancharou/Desktop/ControlHub/experiment_server/dummy/results_exp.m', exp_path)
            #######
            '''

            time.sleep(95)  # 95
            kill_matlab('java.exe')
            time.sleep(5)
            print('Experiment is finished!')

            shutil.move(os.path.join(exp_path, 'error_code_exp.txt'), res_saving_path)
            shutil.move(os.path.join(exp_path, 'performance_exp.txt'), res_saving_path)
            try:
                shutil.move(os.path.join(exp_path, 'results_exp.m'), res_saving_path)
            except:
                print('There is no results_exp.m')
            print('Experiment Results are moved to the user folder!')

            with open(os.path.join(res_saving_path, "error_code_exp.txt")) as error_exp:
                error_msg_exp = error_exp.read()
                if error_msg_exp[0] == "0":
                    print('Experiment is finished without errors!')
                else:
                    print("There is an error in experiment: ", error_msg_exp)

        else:
            print("There is an error in simulation: ", error_msg_sim)
            shutil.copy(os.path.join(res_saving_path, 'performance_sim.txt'), os.path.join(res_saving_path, 'performance_exp.txt'))
    ######
    #for i in listdir_nohidden('C:/Users/Fiodar Hancharou/Desktop/ControlHub/experiment_server/dummy/'):
    #    shutil.copy('C:/Users/Fiodar Hancharou/Desktop/ControlHub/experiment_server/dummy/'+ i, res_saving_path)
    ######
    time.sleep(5)


def make_archive(target, endpath):
    with ZipFile(endpath + '.zip', 'w') as zipObj:
        for folderName, subfolders, filenames in os.walk(target):
            for filename in filenames:
                filePath = os.path.join(folderName, filename)
                # Add file to zip
                zipObj.write(filePath, basename(filePath))


def listdir_nohidden(path):
    files = os.listdir(path)
    return [i for i in files if not re.match('^\.', i)]


def kill_matlab(name):
    ti = 0
    f = wmi.WMI()
    for process in f.Win32_Process():
        if process.name == name:
            process.Terminate()
            ti += 1
    if ti == 0:
        print("Process not found!!!")
