# -*- mode: python; coding: utf-8; -*-

"""Operations with the files on the remote FTP server"""

import ftplib
import os
import re


def ftp_listdir(ip_address, login, password, ftp_directory):
    """
    Returns the list with the files contained in a specified directory of FTP-server
    """
    ftp = ftplib.FTP(ip_address)
    ftp.login(user=login, passwd=password)
    ftp.cwd(ftp_directory)
    files_list = ftp.nlst()
    files_list_nohidden = [i for i in files_list if not re.match('^\.', i)]
    ftp.quit()
    return files_list_nohidden


def ftp_move_from(ip_address, login, password, ftp_directory, ftp_filename, destination):
    """
    Makes the copy of data inside the file and deletes that file from FTP
    """
    ftp = ftplib.FTP(ip_address)  # create an instance of ftp
    ftp.login(user=login, passwd=password)  # login to FTP with login and password
    ftp.cwd(ftp_directory)  # change the directory and open the target file in the FTP
    # create a file and copy the data from FTP to it, after that delete the file in the FTP folder
    with open(os.path.join(destination, ftp_filename), 'wb+') as ffile:
        ftp_dst = 'RETR ' + ftp_filename
        ftp.retrbinary(ftp_dst, ffile.write)
    ftp.delete(ftp_filename)
    ftp.quit()


def ftp_move_to(ip_address, login, password, source_dir, filename, ftp_destination):
    """
    Creates the file on the FTP and writes data to it,
    then deletes the file from the local source
    """
    ftp = ftplib.FTP(ip_address)
    ftp.login(user=login, passwd=password)
    ftp.cwd(ftp_destination)
    with open(os.path.join(source_dir, filename), "rb") as tfile:
        ftp.storbinary("STOR {}".format(filename), tfile)
    ftp.sendcmd(('SITE CHMOD 777 ' + filename))  # change permission to maximum
    ftp.quit()
    os.remove(os.path.join(source_dir, filename))


def ftp_copy_paste_to(ip_address, login, password, source_dir, file_copy, ftp_destination, file_paste):
    """
    Makes a copy of information from the file specified and
    then writes it to the file on the FTP server
    """
    ftp = ftplib.FTP(ip_address)
    ftp.login(user=login, passwd=password)
    ftp.cwd(ftp_destination)
    with open(os.path.join(source_dir, file_copy), "rb") as rfile:
        ftp.storbinary("STOR {}".format(file_paste), rfile)
    # ftp.sendcmd(('SITE CHMOD 777 ' + filename))  # change permission to maximum
    ftp.quit()


def ftp_delete_from(ip_address, login, password, ftp_directory, ftp_filename):
    """
    Deletes a file from the specified folder on the FTP server
    """
    ftp = ftplib.FTP(ip_address)  # create an instance of ftp
    ftp.login(user=login, passwd=password)  # login to FTP with login and password
    ftp.cwd(ftp_directory)
    ftp.delete(ftp_filename)
    ftp.quit()
