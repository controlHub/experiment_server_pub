# -*- mode: python; coding: utf-8; -*-

"""Sending the archive with results from experiment computer to FTP"""

import time
from common.ftp_operations import os, ftp_move_to
from common.exp_processing_operations import listdir_nohidden
from common.variables import project_dir, ip_address, ftp_login, ftp_password, ftp_dst

move_from = os.path.join(project_dir, 'completed_rasp/')


def main():
    while True:
        files = listdir_nohidden(move_from)
        print(files)
        if len(files) == 1:
            time.sleep(5)
        if len(files) > 0:
            ftp_move_to(ip_address, ftp_login, ftp_password, move_from, files[0], ftp_dst)
            print('FTP file uploading is successful!')
        else:
            time.sleep(20)
            print('!')


if __name__ == '__main__':
    main()
