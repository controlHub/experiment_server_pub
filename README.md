# ControlHub. Experiment_server

## Table of contents
* [General info](#general-info)
* [Features](#features)
* [Technologies](#technologies)
* [Setup](#setup)

## General Info
The simple python server working in a loop. It handles the files submitted by the users at the "submission_video_server", runs the experiment, then transmits the experiment results back to the "submission_video_server". Additionaly, it updates the status of the experiment execution shown at the video frame broadcasted by the "submission_video_server".

## Features
- The file transfer implemented using FTP technology
- The algorithm for the automatic database and filestorage cleaning
- The timings of the procedure are controlled by the time triggers

## Technologies
Project is created with:
* Python 3.6 or 3.8
	
## Setup
To run this project, copy it to the local folder, specify your variables in the common/varialbes.py file and run using python 3.6 or higher:

```
$ cd ../experiment_server/
$ python submission_handling.py
$ python result_sending.py
$ python event_updater.py
```
 
